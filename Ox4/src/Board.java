import java.util.Random;
public class Board {
	private char[][] data = new char[4][4];
	private Player currentPlayer;
	private Player o;
	private Player x;
	private Player win;
	
	public Board(Player o,Player x) {
		this.o = o;
		this.x = x;
		initializeBoard();
		randomPlayer();
	}
	public Board() {
		
	}
	public void initializeBoard() {
        for (int i = 1; i < 4; i++) {
            for (int j = 1; j < 4; j++) {
                data[i][j] = '-';
            }
        }
    }
	public void randomPlayer() {
		int rx=0;
		int ro=1;
		Random r = new Random();
		if(r.nextInt(2)==rx) {
			currentPlayer = x;
		}else {
			currentPlayer = o;
		}
	}
	public void setRowCol(int row, int col) {
		data[row][col] = currentPlayer.getName();
	}
	public boolean isBoardFull() {
        boolean isFull = true;
        for (int i = 1; i < 4; i++) {
            for (int j = 1; j < 4; j++) {
                if (data[i][j] == '-') {
                    isFull = false;
                }
            }
        }
        return isFull;
    }
	public boolean checkRowCol(char c1, char c2, char c3) {
		return ((c1 != '-') && (c1 == c2) && (c2 == c3));
	}
	public Player getWinner() {
		return win;
	}
	public void switchTrun() {
		if (currentPlayer.getName() == 'x') {
    		this.currentPlayer = o;
    	}else {
    		this.currentPlayer = x;
    	}
	}
	public Player getCurrentPlayer() {
		return this.currentPlayer;
	}
	public char[][] getData(){
		return data;
	}
	public boolean checkRowsForWin() {
		for (int i = 1; i < 4; i++) {
    		if (checkRowCol(data[i][1], data[i][2], data[i][3]) == true) {
    			return true;
    		}
    	}
    	return false;
	}
	public boolean checkColumnsForWin() {
		for (int i = 1; i < 4; i++) {
    		if (checkRowCol(data[1][i], data[2][i], data[3][i]) == true) {
    			 return true;
    		}
    	}
    	return false;
	}
	public boolean checkDiagonalsForWin() {
		return ((checkRowCol(data[1][1], data[2][2], data[3][3]) == true) || (checkRowCol(data[1][3], 
    			data[2][2], data[3][1]) == true));
	}
	public boolean checkForWin() {
		if(checkRowsForWin() || checkColumnsForWin() || checkDiagonalsForWin()){
			this.win = currentPlayer;
			return true;
		}
		return false;
	}
	public void updateStat() {
		if(o == this.getWinner()) {
			this.o.countWin();
			this.x.countLose();
		}else if(x == this.getWinner()) {
			this.x.countWin();
			this.o.countLose();
		}else {
			this.o.countDraw();
			this.x.countDraw();
		}
	}
	
}
