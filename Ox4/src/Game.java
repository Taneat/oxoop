import java.util.Scanner;
public class Game {
	private Board board;
	private int row;
	private int col;
	private Player o;
	private Player x;
	private char con;
	
	Scanner kb = new Scanner(System.in);
	public Game () {
		o = new Player('o');
		x = new Player('x'); 
	}
	public void startGame() {
		board = new Board(o,x); 
	}
	public void showWelcome() {
		System.out.println("Welcome to OX Game");
	}
	public void printCurrentBoard() {
		System.out.println("Current board");
	}
	public void showBoard() {
    	System.out.println("-------------");
    	for (int i = 1; i < 4; i++) {
    		System.out.print("| ");
    		for (int j = 1; j < 4; j++) {
    			System.out.print(board.getData()[i][j] + " | ");
    	    }
    		System.out.println();
    		System.out.println("-------------");
    	 }
    }
	public boolean inputRowCol() {
		while(true) {
			System.out.print("Please input row,column : ");
			row=kb.nextInt();
			col=kb.nextInt();
			if ((row >= 1) && (row < 4)) {
				if ((col >= 1) && (col < 4)) {
					if (board.getData()[row][col] == '-') {
						board.setRowCol(row, col);
		   				 return true;
		   			 }
		   		 }
			}
			System.out.println("Please input 1-3!!!");
			return false;
		}
		
	}
	public boolean inputContinue() {
		System.out.print("input Continue(y/n) : ");
		con = kb.next().charAt(0);
		if(con=='y') {
			this.startGame();
			return true;
		}else {
			return false;
		}
	}
	public void showWin(Player player) {
		System.out.println("Player "+player.getName()+ " win!!");
	}
	public void showTurn() {
		System.out.println(board.getCurrentPlayer().getName()+ " Trun");
	}
	public void runOnce() {
		while(true) {
			this.showBoard();
			this.showTurn();
			if(this.inputRowCol()) {
				if(board.isBoardFull()) {
					this.showBoard();
					board.updateStat();
					System.out.println("Draw!!!");
					this.showStat();
					return;
				}
				if(board.checkForWin()) {
					this.showBoard();
					board.updateStat();
					Player player = board.getCurrentPlayer();
					this.showWin(player);
					this.showStat();
					return;
				}
				board.switchTrun();
			}
		}
	}
	public void run() {
		board = new Board(o , x);
		this.showWelcome();
		this.printCurrentBoard();
		while(true) {
			this.runOnce();
			if(!this.inputContinue()){
				return;
			}	
		}
	}
	public void showStat() {
		System.out.println(o.getName()+"(win,lose,draw) : "+o.getWin()+","+o.getLose()+","+o.getDraw());
		System.out.println(x.getName()+"(win,lose,draw) : "+x.getWin()+","+x.getLose()+","+x.getDraw());
	}
}
